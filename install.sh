
# Download Latest Firefox Developer Edition

cd ~/Downloads
wget "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-US" -O firefox.tar.bz2
# Move the file to /opt directory
sudo mv firefox.tar.bz2 /opt/

# go to the /opt directory
cd /opt
# Extract the file
sudo tar xjf firefox.tar.bz2
# remove the downloaded file, no longer needed.
sudo rm -f firefox.tar.bz2

# Ensure the user has access to the binary to launch the browser
sudo chown -R $USER /opt/firefox


# Add to Bash
echo "export PATH=/opt/firefox/firefox:$PATH" >> ~/.bashrc
# Update terminal Bash Profile
source ~/.bashrc

# Create Desktop file
cat > ~/.local/share/applications/firefoxDeveloperEdition.desktop <<EOL
[Desktop Entry]
Encoding=UTF-8
Name=Firefox Developer Edition
Exec=/opt/firefox/firefox
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Terminal=false
Type=Application
Categories=Network;WebBrowser;Favorite;
MimeType=text/html;text/xml;application/xhtml_xml;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp; X-Ayatana-Desktop-Shortcuts=NewWindow;NewIncognitos;
EOL
