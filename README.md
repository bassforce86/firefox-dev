# Firefox Developer Edition

This is a small script to automate the installation of Firefox Developer Edition

## This script uses sudo commands, please proceed with caution.
to run, clone this repo and run `./install.sh`.

If you don't need all the things this does - then just pick out the parts you do need. 

It's just a convenience script I wrote for myself, but feel free to use and suggest improvements!
